# Eternalfest game result viewer

[https://gameresult.ef.rainbowda.sh/](https://gameresult.ef.rainbowda.sh)

```sh
npm install
mv config.json.default config.json
./genToken.sh USERNAME PASSWORD # SERVER (this last parameter is optional)
$EDITOR config.json
npm start
```
